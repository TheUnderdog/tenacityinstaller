apt-get install -y build-essential libavcodec-dev libavformat-dev libavutil-dev libflac++-dev libglib2.0-dev libgtk-3-dev libid3tag0-dev libjack-dev liblilv-dev libmad0-dev libmp3lame-dev libogg-dev libpng-dev portaudio19-dev libportmidi-dev libserd-dev libsndfile1-dev libsord-dev libsoundtouch-dev libsoxr-dev libsuil-dev libtwolame-dev vamp-plugin-sdk libvorbis-dev lv2-dev zlib1g-dev cmake ninja-build libjpeg-dev libtiff-dev liblzma-dev libsqlite3-dev libwxgtk3.0-gtk3-dev cmake zip git wget
apt-get install -y ccache ninja-build
cd ~/Downloads
wget -O Tenacity.zip https://github.com/tenacityteam/tenacity/archive/refs/heads/master.zip
unzip Tenacity.zip
cd tenacity-master
cmake -G Ninja -S . -B build
#cmake --build build
#cmake --install build
